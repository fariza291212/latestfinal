<?php
include("config.php");
$return_arr = array();
$results = $mysqli->query("SELECT product_code, product_name, product_desc, product_img_name, price FROM products ORDER BY id ASC");
if($results){ 
	while($obj = $results->fetch_object()){
		$return_arr[] = array("product_code"=>$obj->product_code,"product_name"=>trim(preg_replace('/\s\s+/', ' ',$obj->product_name)),"product_desc"=>$obj->product_desc,"product_img_name"=>trim(preg_replace('/\s\s+/', ' ', $obj->product_img_name)),"price"=>$obj->price);
	}
}
echo json_encode($return_arr);
?>