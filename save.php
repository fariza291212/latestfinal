<?php
session_start();
include_once("config.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>View</title>
<link href="style/style.less" rel="stylesheet/less"/>
<script src="style/less.min.js"></script>
<link href="style/style3.less" rel="stylesheet/less"/>
</head>
<body>
<div id="wrapping" class="wrap">
		<a id="basy"href="sushi.php"><h4 id="a" >Лучшая кухня на суше,безусловно у нас<img id="burg" align="bottom" src="logos.png">
		Профессиональный взгляд на японскую кухню</h4></a>
<h1 align="center">View Cart</h1>
<div class="cart-view-table-back">
<form method="post" action="cart_update.php">
<table width="100%"  cellpadding="6" cellspacing="0" style="border-radius:5px;">
<thead><tr><th>ID</th><th>Name</th><th>Total Price</th></tr></thead>
  <tbody>
 	<?php
	if(isset($_SESSION["cart_products"])) {
		$total = 0; 
		$b = 0; 
		foreach ($_SESSION["cart_products"] as $cart_itm){
			$product_name = $cart_itm["product_name"];
			$product_qty = $cart_itm["product_qty"];
			$product_price = $cart_itm["product_price"];
			$product_code = $cart_itm["product_code"];
			$subtotal = ($product_price * $product_qty); 
				
		   	$bg_color = ($b++%2==1) ? 'odd' : 'even'; 
		    echo '<tr class="'.$bg_color.'">';
			echo '<td>1</td>';
			echo '<td>'.$product_name.'</td>';
			echo '<td>'.$subtotal.'tg'.'</td>';
			
            echo '</tr>';
			$total = ($total + $subtotal); 
        }
	}
    ?>
    <tr><td colspan="5"><a href="index.php" class="button">Add More Items</a><button name="submit" type="submit">Update</button></td></tr>
  </tbody>
</table>
<input type="hidden" name="return_url" value="<?php 
$current_url = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
echo $current_url; ?>" />
</form>
</div>
</div>
</body>
</html>
