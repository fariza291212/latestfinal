<?php
session_start();
include_once("config.php");
$current_url = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
?>
<!DOCTYPE html>
<html>
<head>
<link href="style/style.less" rel="stylesheet/less"/>
<script src="jquery-2.2.0.js"></script>
<script src="angular.min.js"></script>
<script type="text/javascript" src="angular-route.js"></script>
<script type="text/javascript" src="angular-animate.js"></script>
<script src="functions.js"></script>
<script src="style/less.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Shopping Cart</title>
<script type="text/javascript" src=""></script>
</head>
<body ng-app="myApp" ng-controller="fromCtrl">
<div id="wrapping" class="wrap">
		<a id="basy"href="sushi.php"><h4 id="a" >Лучшая кухня на суше,безусловно у нас<img id="burg" align="bottom" src="style/logos.png">
		Профессиональный взгляд на японскую кухню</h4></a>
		<div id="us">
		<p id="usl">Условия доставки</p>
		</div>
		<div id="cc" style="margin-left:200px;">
		<p id="c">Минимальная цена</p>
		<p id="baga">3000 тг</p>
		</div>
		<div id="st">
		<p id="stt"> Стоимость доставки</p>
		<p id="b"> Бесплатно</p>
		</div>
		<div id="sr">
		<p id="srr">Среднее время доставки</p>
		<p id="sra">30 минут</p>
		</div>
		<br/>
		<br/><br/><br/>
		<table id="tabs" style="margin-left:-200px;">
			<tr>
				<td><a href="view_cart.php"><img class="k" height:"480px" width="480px "src="style/dostavka.png"></a></td>
				
			</tr>
		</table>
		<p class="search"><input type="text" placeholder="search" ng-model="query"></p>
		<div class="cart-view-table-front" id="view-cart" ng-if="bool">
			<h3>Your Shopping Cart</h3>
			<table width="100%"  cellpadding="6" cellspacing="0" >
				<tbody>

					<tr class="{{x.bg_color}}" ng-repeat="x in cart">
						<td>Qty <input type="text" size="2" maxlength="2" name="product_qty['{{x.product_code}}']" value="'{{x.product_qty}}'" /></td>
						<td>{{x.product_name}}</td>
						<td><button ng-click="removeElement(x.product_code)">Remove</button></td>
					</tr>
					<td colspan="4">
						<button type="submit">Update</button><a href="view_cart.php" class="button">Checkout</a>
					</td>
				</tbody>
			</table>
			<input type="hidden" name="return_url" value="'<?=$current_url?>'" />
		</div>
		<ul class="products">
			<li class="product" ng-repeat="x in sushi | filter:query">
				<div class="product-content"><h3>{{x.product_name}}</h3>
					<div class="product-thumb"><img style="width:250px; height:250px;" src="images/{{x.product_img_name}}"></div>
					<div class="product-desc">{{x.product_desc}}</div>
					<div class="product-info">
					Price {{x.price}} tg
					<fieldset>
						<label>
							<span>Quantity</span>
							<input type="text" size="2" maxlength="2" name="product_qty" value="1" ng-model="value"/>
						</label>
					</fieldset>
					<input type="hidden" name="product_code" value="{{x.product_code}}" ng-model="product_code"/>
					<input type="hidden" name="type" value="add" ng-model="type"/>
					<input type="hidden" name="return_url" value="<?=$current_url?>" ng-model="return_url"/>
					<div align="center"><button type="submit" class="add_to_cart" ng-click="updateCart(x.product_code,'add','<?=$current_url?>',value,$index)">Add</button></div>
					</div>
				</div>
			</li>
		</ul>
</body>
</html>
