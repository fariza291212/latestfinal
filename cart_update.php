<?php
session_start();
include_once("config.php");
if(isset($_SESSION["cart_products"])){
	$new_array = $_SESSION["cart_products"];
}else{
	$new_array = array();
}
if(isset($_GET["type"]) && $_GET["type"]=='add' && $_GET["product_qty"]>0){
	foreach($_GET as $key => $value){ 
		$new_product[$key] = filter_var($value, FILTER_SANITIZE_STRING);
    }

	unset($new_product['type']);

    $statement = $mysqli->prepare("SELECT product_name, price FROM products WHERE product_code=? LIMIT 1");
    $statement->bind_param('s', $new_product['product_code']);
    $statement->execute();
    $statement->bind_result($product_name, $price);
	
	while($statement->fetch()){

        $new_product["product_name"] = trim(preg_replace('/\s\s+/', ' ',$product_name)); 
        $new_product["product_price"] = $price;
        
        if(isset($new_array["cart_products"])){ 
            if(isset($new_array[$new_product['product_code']])) 
            {
                unset($new_array["cart_products"][$new_product['product_code']]); 
            }           
        }
        $new_array[$new_product['product_code']] = $new_product; 
    
    }
    $final_array = array();
    foreach ($new_array as $value) {
    	$final_array[] = $value;
    }
 
}
$_SESSION["cart_products"] = $new_array;
echo json_encode($final_array);

/*$return_url = (isset($_POST["return_url"]))?urldecode($_POST["return_url"]):''; 
header('Location:'.$return_url);*/

/*if(isset($_POST["product_qty"]) || isset($_POST["remove_code"])){
	if(isset($_POST["product_qty"]) && is_array($_POST["product_qty"])){
		foreach($_POST["product_qty"] as $key => $value){
			if(is_numeric($value)){
				$_SESSION["cart_products"][$key]["product_qty"] = $value;
			}
		}
	}

	if(isset($_POST["remove_code"]) && is_array($_POST["remove_code"])){
		foreach($_POST["remove_code"] as $key){
			unset($_SESSION["cart_products"][$key]);
		}	
	}
}*/
?>