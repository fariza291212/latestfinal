<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
	<title>Рецепты</title>
	<link href="style/style1.less" rel="stylesheet/less"/>
	<script src="style/less.min.js"></script>
	<script src="jquery.min.js" ></script>
	<script>
	$(document).ready(function(){
		$("#fariza6").hide();
		$("#fariza5").hide();
		$("#fariza4").hide();
		$("#fariza3").hide();
		$("#fariza2").hide();
		$("#fariza").hide();

	$("#fila").click(function(){
		$("#fariza").show(1000);
	});
	$("#text").click(function(){
		$("#fariza2").show(1000);
		
	});
	$("#text2").click(function(){
		$("#fariza3").show(1000);
		
	});
	$("#text3").click(function(){
		$("#fariza4").show(1000);
		
	});
	$("#text4").click(function(){
		$("#fariza5").show(1000);
		
	});
	$("#text5").click(function(){
		$("#fariza6").show(1000);
		
	});
	$("#exit1").click(function(){
		$('#fariza3').hide();

	});
	$("#exit").click(function(){
		$('#fariza').hide();

	});
	$("#exit2").click(function(){
		$('#fariza2').hide();

	});
	$("#exit3").click(function(){
		$('#fariza4').hide();

	});
	$("#exit4").click(function(){
		$('#fariza5').hide();

	});
	$("#exit5").click(function(){
		$('#fariza6').hide();

	});
	});
	
	</script>
</head>
<body>
	<div id="wrapping" class="wrap">
		<a id="basy"href="sushi.php"><h4 id="a" >Лучшая кухня на суше,безусловно у нас<img id="burg" align="bottom" src="logos.png">
		Профессиональный взгляд на японскую кухню</h4></a>
		<p id="zag"><b><i>Рецепты суш и роллов</i></b></p>
		<div id="fil2">
	    	<h3 class="hh" > <a id="fila"  style="color:blue;cursor:pointer;">Филадельфия</a></h3>
	    	<p class="pp">Ролл "филадельфия" - это один из самых известных и популярных во всем мире роллов. В своем фото-рецепте я покажу Вам, как быстро и просто научится делать такой ролл в домашних условиях, используя доступные продукты.</p>
		</div>
		<div id="fariza">
			<img src="exit.png" width="17px" height="17px" id="exit">
			<img src="recep.jpg" width="220px" height="180" class="recep">
				<p class="recept">
					Вам потребуются:

рис — 2 стакана;
вода — 2,5 стакана;
семга (можно заменить форелью) — 600 г;
спелый авокадо (по желанию);
огурец (по желанию);
Суши филадельфия с авокадорисовый уксус — 50 мл;
сахар — 2 столовые ложки;
соль — столовая ложка;
нори листовые — 3 шт.;
сливочный сыр Филадельфия — 300 г;
васаби, маринованный имбирь и соевый соус по вкусу.
				</p>
		</div>
		<div id="fariza2">
			<img src="exit.png" width="17px" height="17px" id="exit2">
				<img src="recep1.jpg" width="220px" height="180" class="recep">
				<p class="recept">
					Вам потребуются:

Рис нишики	150 г
Уксус для суши	2 столовые ложки
Мясо краба	175 г
Авокадо	100 г
Майонез японский	50 мл
Икра тобико	100 г
Сухие водоросли нори	5 штук
Васаби	по вкусу
Маринованный имбирь	по вкусу.
				</p>
		</div>
		<div id="fariza3">
			<img src="exit.png" width="17px" height="17px" id="exit1">
				<img src="recep2.jpg" width="220px" height="180" class="recep">
				<p class="recept">
					Вам потребуются:

Рис для суши
тесто для Харумаки (спринг роллов)
семга (лосось)
икра летучей рыбы
сыр "Буко"
Яйцо куриное
				</p>
		</div>
		<div id="fariza4">
			<img src="exit.png" width="17px" height="17px" id="exit3">
				<img src="recep3.jpg" width="220px" height="180" class="recep">
				<p class="recept">
					Вам потребуются:

Рис для суши
нори
семга (лосось)
угорь
сыр "Буко"
имбирь
огурец
соус "Унаги"
				</p>
		</div>
		<div id="fariza5">
			<img src="exit.png" width="17px" height="17px" id="exit4">
				<img src="recep4.jpg" width="220px" height="180" class="recep">
				<p class="recept">
					Вам потребуются:

нори
Рис для суши
огурец
угорь
мясо краба
икра летучей рыбы
				</p>
		</div>
		<div id="fariza6">
		<img src="exit.png" width="17px" height="17px" id="exit5">
				<img src="recep5.jpg" width="220px" height="180" class="recep">
				<p class="recept">
					Вам потребуются:

Рис для суши (японский, круглозерный)	200 г	Уксус рисовый	20 мл
Икра летучей рыбы (тобико)	30 г	Водоросли нори (большие листы)	1 шт.
Огурцы	100 г	Семга	60 г


				</p>
		</div>
		<div id="fil">
			<img id="im" src="f.jpg">
		</div>
		<div id="fil3">
	    	<h3 class="hh" id="text"><a id="fila" style="color:blue;cursor:pointer;"> Калифорния</a></h3>
	    	<p class="pp">Ролл «Калифорния» — разновидность суши, приготовляемая вывернутым рисом наружу. В своем фото-рецепте я покажу Вам, как быстро и просто научится делать такой ролл в домашних условиях, используя доступные продукты. </p>
		</div>
		<div id="fil">
			<img id="im" src="kali.jpg">
		</div>
		<div id="fil3">
	    	<h3 class="hh" id="text2"><a id="fila" style="color:blue;cursor:pointer;">Харумаки роллы</a></h3>
	    	<p class="pp">Суши-гранат. Японская кухня щедра на выдумку!Вот и я сегодня хочу вам предложить немного непривычные суши. Эти суши похожие на бутон цветка или гранат.</p>
		</div>
		<div id="fil">
			<img id="im" src="tep.jpg">
		</div>
		<div id="fil3">
	    	<h3 class="hh" id="text3"><a id="fila" style="color:blue;cursor:pointer;">Темпура ролл</a></h3>
	    	<p class="pp">В этот раз решили приготовить маленькие горячие роллы в темпуре - из половины листа нори и всего из трех начинок. </p>
		</div>
		<div id="fil">
			<img id="im" src="tem.jpg">
		</div>
		<div id="fil3">
	    	<h3 class="hh" id="text4"><a id="fila" style="color:blue;cursor:pointer;">Салют ролл</a></h3>
	    	<p class="pp"> Название роллу дала разноцветная икра летучей рыбы. При приготовлении этого ролла есть небольшая хитрость. На нори мы сначала выкладываем тонко порезанный огурец и только потом уже на огурец кладем рис. </p>
		</div>
		<div id="fil">
			<img id="im" src="ovo.jpg">
		</div>
		<div id="fil3">
	    	<h3 class="hh" id="text5"><a id="fila" style="color:blue;cursor:pointer;">Мозаика ролл</a></h3>
	    	<p class="pp">Ролл мозаика привлекает своим необычным видом и способом приготовления. Мозаикой его называют потому, что он состоит из нескольких частей, скрепленных вместе в единое целое. </p>
		</div>
		<div id="fil">
			<img id="im" src="moz.jpg">
		</div>
<div>
	</div>
</body>
</html>
