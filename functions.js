var app = angular.module('myApp',[]);
app.controller('fromCtrl',function($scope,$http){
	$scope.value = 1;
	$scope.bool = false;
	$scope.total = 0;
	$scope.cart = [];
	$http.get("http://localhost/fari_sushi/getSushi.php").then(function(response){
		//$scope.sushi = JSON.parse(response.data);
		$scope.sushi = response.data;
	});
	$http.get("http://localhost/fari_sushi/get_cart.php").then(function(response){
		$scope.cart = response.data;
		if($scope.cart.length>0){
			$scope.bool = true;
		}
	});
	$scope.getTotal = function(){
	    var total = 0;
	    for(var i = 0; i < $scope.cart.length; i++){
	        var product = $scope.cart[i];
	        total += (product.product_price * product.product_qty);
	    }
	    return total;
	}
	$scope.saveOrder = function(){
	    var total = 0;
	    var goods = "";
	    for(var i = 0; i < $scope.cart.length; i++){
	        var product = $scope.cart[i];
	        total += (product.product_price * product.product_qty);
	        goods += product.product_name+",";
	    }
	    $http.get("http://localhost/fari_sushi/storeOrder.php?goods="+goods+"&total="+total).then(function(response){
			$scope.cart = response.data;
			if($scope.cart.length>0){
				$scope.bool = true;
			}
		});
	}
	$scope.updateCart = function(x,y,z,a,ind){
		$http.get("http://localhost/fari_sushi/cart_update.php?product_code="+x+"&type="+y+"&return_url="+z+"&product_qty="+a).then(function(response){
			$scope.cart = response.data;
			if($scope.cart.length>0){
				$scope.bool = true;
			}
		});
	}
	$scope.removeElement = function(x){
		$http.get("http://localhost/fari_sushi/cart_delete.php?product_code="+x).then(function(response){
			$scope.cart = response.data;
			if($scope.cart.length==0){
				$scope.bool = false;
			}
		});
	}
});
var app1 = angular.module('recipesApp',[]);
app1.controller('fromCtrl',function($scope,$http){
	$scope.recipes = [];
	$http.get("http://localhost/fari_sushi/getRecipes.php").then(function(response){
		//$scope.sushi = JSON.parse(response.data);
		$scope.recipes = response.data;
		//console.log(response.data);
	});
});