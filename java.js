var excellent = false;
function result() {
	var login = document.getElementById('login').value;
	var pass1 = document.getElementById('pass1').value;
	var pass2 = document.getElementById('pass2').value;
	var email = document.getElementById('email').value;
	var checkbox = document.getElementById('checkbox').checked;
	var re = /[a-z0-9\.\-]+@[a-z0-9]+\.[a-z]+/;
    
	if(login.length<7){
		document.getElementById('one').innerHTML="Input must be minimum 7 characters";
		return excellent = false;
	}
	else{document.getElementById('one').innerHTML="";}

	if (login == login.toLowerCase()) {
 			document.getElementById('two').innerHTML="Input must have minimum one Upper character";
			return excellent = false;
	}
	else{document.getElementById('two').innerHTML="";}
	
	if (login.search(/[0-9]/i)==-1){
		document.getElementById('seven').innerHTML="Not enough number, must 1 number";
		return excellent = false;
	}
	else{document.getElementById('seven').innerHTML="";}
	if(checkbox==false){
			document.getElementById('three').innerHTML="Checkbox must be checked";
			return excellent = false;
	}
	else{document.getElementById('three').innerHTML="";}
	if(pass1=="" | pass2==""){
			document.getElementById('six').innerHTML="Passwords must be non empty";
			return excellent = false;
	}
	else{document.getElementById('six').innerHTML="";}
	
	if(pass1!=pass2 | pass1=="" | pass2==""){
			document.getElementById('four').innerHTML="Passwords must be same";
			return excellent = false;
	}
	else{document.getElementById('four').innerHTML=""}

	if(!re.test(email) | email==""){
			document.getElementById('five').innerHTML="Email is invalid";
			return excellent = false;
	}
	else{document.getElementById('five').innerHTML=""; }
	return excellent = true;
	
	}
